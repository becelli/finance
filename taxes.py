class IRRegressiveTax:
    @staticmethod
    def get_tax_rate(days_to_invest: int):
        if days_to_invest <= 180:
            return 22.5 / 100
        elif days_to_invest <= 360:
            return 20.0 / 100
        elif days_to_invest <= 720:
            return 17.5 / 100
        else:
            return 15.0 / 100

class PoupançaRate:
    @staticmethod
    def get_rate(selic_rate: float):
        if selic_rate < 8.5:
            return 0.5 / 100
        else:
            return 0.7 * selic_rate