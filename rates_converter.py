import numpy as np

class RatesConverter:
    @staticmethod
    def yearly_to_monthly(annual_rate: float):
        monthly_rate = np.power(1 + annual_rate, 1/12) - 1
        return monthly_rate
    
    @staticmethod
    def discover_annual_rate(initial_value: float, final_value: float, months: int, monthly_investment: float = 0) -> float:
        if monthly_investment == 0:
            rate = np.power(final_value / initial_value, 12 / months) - 1
            return np.round(rate, 4)

        days_in_months = months / 30
        initial_value = initial_value + monthly_investment * days_in_months
        rate = np.power(final_value / initial_value, 12 / months) - 1
        return np.round(rate, 4)