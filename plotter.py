from __future__ import annotations
from typing import List
from dataclasses import dataclass
import matplotlib.pyplot as plt
from investments import Investment
import numpy as np

    
@dataclass
class InvestmentPlotter:
    investments: List[Investment]
    months: int
    consider_inflation: bool = True
    fig_size = (10, 6)
    dpi = 160

    def plot(self: InvestmentPlotter):
        self.plot_normal()
        self.plot_bar()

    def plot_normal(self: InvestmentPlotter):
        plt.figure(figsize=self.fig_size, dpi=self.dpi)

        for investment in self.investments:
            earned = investment.calculate(self.consider_inflation)
            label = self.get_label(investment)
            plt.plot(range(self.months + 1), earned, label=label)


        
        plt.title(f'Investment evolution for {self.months} months')
        plt.xlabel('Months', position=(0, 1), ha='left')
        plt.ylabel('Money')

        plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=1)
        plt.show()

    def plot_bar(self: InvestmentPlotter):
        plt.figure(figsize=self.fig_size, dpi=self.dpi)

        total_bars = len(self.investments)
        for index, investment in enumerate(self.investments):
            earned = investment.calculate(self.consider_inflation)
            label = self.get_label(investment)
            size = 0.8 / total_bars
            offset = size * index
            plt.bar(np.arange(self.months + 1) + offset, earned, width=size, label=label)
            

        
        plt.title(f'Investment evolution for {self.months} months')
        plt.xlabel('Months', position=(0, 1), ha='left')
        plt.ylabel('Money')


        plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), shadow=True, ncol=1)
        plt.show()
    
    def get_label(self: InvestmentPlotter, investment: Investment) -> str:
        name = investment.get_name()
        rate = f"{investment.annual_rate * 100:.2f}%"
        deposits = f"{investment.monthly_deposits:.2f}/month"
        total = f"Total: {investment.get_total_at_end_of_period(self.consider_inflation):.2f}"
        earnings = f"Profit: {investment.get_earnings_at_end_of_period(self.consider_inflation):.2f}"
        return f"{name} at {rate} a.a. with {deposits} ({total} | {earnings})"
