from __future__ import annotations

import numpy as np

from taxes import IRRegressiveTax
from rates_converter import RatesConverter
from dataclasses import dataclass
from abc import ABC, abstractmethod

@dataclass
class Investment(ABC):
    initial_value: float
    annual_rate: float
    annual_inflation_rate: float
    months: int
    monthly_deposits: float
    name: str | None = None

    @abstractmethod
    def calculate(self: Investment, consider_inflation: bool) -> np.ndarray:
        pass

    def total_deposit_until_now(self: Investment, month: int) -> float:
        return self.monthly_deposits * month + self.initial_value

    def get_name(self: Investment) -> str:
        return self.name if self.name is not None else self.__class__.__name__

    def get_total_at_month(self: Investment, month: int, consider_inflation: bool = True) -> float:
        return self.calculate(consider_inflation)[month]
    
    def get_earnings_at_month(self: Investment, month: int, consider_inflation: bool = True) -> float:
        return self.get_total_at_month(month, consider_inflation) - self.total_deposit_until_now(month)

    def get_total_at_end_of_period(self: Investment, consider_inflation: bool = True) -> float:
        return self.get_total_at_month(self.months, consider_inflation)
    
    def get_earnings_at_end_of_period(self: Investment, consider_inflation: bool = True) -> float:
        return self.get_total_at_end_of_period(consider_inflation) - self.total_deposit_until_now(self.months)

    

@dataclass
class CDB(Investment):
    def calculate(self: CDB, consider_inflation: bool) -> np.ndarray:
        # Calculate the investment evolution for each month
        earned = np.zeros(self.months + 1)
        earned[0] = self.initial_value

        monthly_rate = RatesConverter.yearly_to_monthly(self.annual_rate)
        monthly_inflation_rate = RatesConverter.yearly_to_monthly(self.annual_inflation_rate)
        for month in range(1, self.months + 1):
            if consider_inflation:
                earned[month] = earned[month - 1] * (1 + monthly_rate - monthly_inflation_rate) + self.monthly_deposits
            else:
                earned[month] = earned[month - 1] * (1 + monthly_rate) + self.monthly_deposits



        # Discount the IR tax
        for month in range(1, self.months + 1):
            days_to_invest = month * 30
            tax_rate = IRRegressiveTax.get_tax_rate(days_to_invest)
            # Only discount from the profit. The initial value is not taxed, neither the monthly deposits
            invested_until_now = self.total_deposit_until_now(month)
            profit = earned[month] - invested_until_now
            earned[month] = invested_until_now + profit * (1 - tax_rate)
            
            
        
        return earned

@dataclass
class LCI_LCA(Investment):
    def calculate(self: LCI_LCA, consider_inflation: bool) -> np.ndarray:
        # Calculate the investment evolution for each month
        earned = np.zeros(self.months + 1)
        earned[0] = self.initial_value

        monthly_rate = RatesConverter.yearly_to_monthly(self.annual_rate)
        monthly_inflation_rate = RatesConverter.yearly_to_monthly(self.annual_inflation_rate)
        for month in range(1, self.months + 1):
            if consider_inflation:
                earned[month] = earned[month - 1] * (1 + monthly_rate - monthly_inflation_rate) + self.monthly_deposits
            else:
                earned[month] = earned[month - 1] * (1 + monthly_rate) + self.monthly_deposits
        
        return earned


@dataclass
class Inflation(Investment):
    def calculate(self: Inflation, consider_inflation: bool) -> np.ndarray:
        # Calculate the investment evolution for each month
        earned = np.zeros(self.months + 1)
        earned[0] = self.initial_value

        monthly_inflation_rate = RatesConverter.yearly_to_monthly(self.annual_inflation_rate)
        for month in range(1, self.months + 1):
            if consider_inflation:
                earned[month] = earned[month - 1] * (1 - monthly_inflation_rate) + self.monthly_deposits
            else:
                earned[month] = earned[month - 1] + self.monthly_deposits
        
        return earned

@dataclass
class Poupança(Investment):
    def calculate(self: Poupança, consider_inflation: bool) -> np.ndarray:
        # Calculate the investment evolution for each month
        earned = np.zeros(self.months + 1)
        earned[0] = self.initial_value

        monthly_rate = RatesConverter.yearly_to_monthly(self.annual_rate)
        monthly_inflation_rate = RatesConverter.yearly_to_monthly(self.annual_inflation_rate)
        for month in range(1, self.months + 1):
            if consider_inflation:
                earned[month] = earned[month - 1] * (1 + monthly_rate - monthly_inflation_rate) + self.monthly_deposits
            else:
                earned[month] = earned[month - 1] * (1 + monthly_rate) + self.monthly_deposits
        
        return earned